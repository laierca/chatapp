
import React, { Component } from 'react'
import dynamic from 'next/dynamic';
import fetch from 'isomorphic-unfetch'

const MySidebar = dynamic(() => import('../components/Sidebar'), {
  ssr: false
});

export default class componentName extends Component {
  constructor(props) {
    super(props)

    this.state = {
      messages: '',
      myMessages: '',
      fetchOK: ''
    }
  }

  componentDidMount() {
    fetch('https://lajerca.website/__CMS/WEBDEV/api/collections/get/SimpleChat', {

      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
    }



    )
      .then(res => { if (res.ok) { return (res.json()) } else { this.setState({ fetchOK: false }) } })
      .then(json => this.setState({ messages: json, fetchOK: true }))
      .catch(function (error) {
        console.log('FETCH MESSAGES ERROR', error)
      })
  }

  render() {
    console.log('index.js || INSIDE RENDER || this.state', this.state);

    if (this.state.fetchOK) {
      return (
        <div>
          <MySidebar data={this.state.messages.entries} />
        </div>
      )

    } else {
      return (<div>  <h1> Messages not loaded  </h1></div>)
    }

  }
}
