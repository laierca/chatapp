import React, { Component } from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

export default class MyModal extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modalOpen: false
        }
        
        this.handleClose = this.handleClose.bind(this)
    }
    


    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })
    handleYes = () => {
       this.props.handleDelete()
        this.setState({ modalOpen: false })
    }


    render() {

        console.log('MODAL', this.props);
        
        return (
            <Modal
                trigger={

                    <Icon
                        onClick={this.handleOpen}
                        circular name='trash alternate outline'
                        color='red'
                        link
                    />

                }
                open={this.state.modalOpen}
                onClose={this.handleClose}
                basic
                size='tiny'
            >
                <Header icon='trash' content='Delete Message' />
                <Modal.Content>
                    <h3>Are you sure?</h3>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.handleClose} inverted>
                        <Icon name='remove' /> No
                    </Button>
                    <Button color='green' onClick={this.handleYes } inverted>
                        <Icon name='checkmark' /> Yes
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

