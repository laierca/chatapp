import React, { Component } from 'react'

import { MessageBox } from 'react-chat-elements';
import Avatar from 'react-avatar';

import DateHandler from './DateHandler';
//CSS
import 'react-chat-elements/dist/main.css';
import './UserMessage.css'


export default class UserMessage extends Component {

    dateFunc() {
        const date = DateHandler({ date: this.props.timestamp })
        return date
    }

   
    render() {
        console.log('CURRENT USER MESSAGE', this.props);

        // var d = new Date('2019-03-25')
        return (
            <div className='user_message'>

                <div className='message_avatar_right'>
                    <Avatar name={this.props.author} size={50} round />
                </div>
                <div className='message_box_right'>
                    <MessageBox
                        position={'right'}
                        type={'text'}
                        text={this.props.message}
                        title={this.props.author}
                        titleColor='grey'
                        status='received'
                        dateString={this.dateFunc()}
                    //date={d}
                    />
                 
                </div>
            </div>
        )
    }
}

