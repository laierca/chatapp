import React, { Component } from 'react'
//COMPONENTS
import TopBar from './TopBar';
import Messages from './Messages';
import BottomBar from './BottomBar';
//CSS
import './ChatBody.css'

export default class ChatBody extends Component {
    constructor(props) {
        super(props)

        this.state = {
            myMessages: [
                { author: 'Current User', message: 'fake message from state', _created: '1566755524' },
                { author: 'Current User', message: 'fake sent message', _created: '1566599243' }]
        }
        this.handleSentMessage = this.handleSentMessage.bind(this)
    }

    handleSentMessage = (data) => {
        console.log('HANDLE SENT MESSAGE', data);
        this.setState(prevState => ({ myMessages: prevState.myMessages.concat(data) }))
    }

    render() {
        return (
            <div>
                <div className='top_bar_wrapper'>
                    <TopBar />
                </div>
                <div className='messages_wrapper' >
                    <Messages myMessages={this.state.myMessages} data={this.props.data} />
                </div>
                <div className='bottom_bar_wrapper'>
                    <BottomBar handleSentMessage={ this.handleSentMessage } />
                </div>
            </div>
        )
    }
}
