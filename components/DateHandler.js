
var timestamp = require('unix-timestamp')

const DateHandler = (props) =>  {
    console.log('DATE HANDLER', props);
    

    const customDate =  props.date || '1561536257761'
    const customDateString =  String(customDate) || '1561536257761'

    const datestring  =   customDateString.length > 10 ?   customDateString.substring(0, 10) : customDateString

    const dateOK = parseInt(datestring)
    const formatDate = timestamp.toDate(dateOK)
    const dateCropOK = String(formatDate).substring(4, 21)

    if (props.date) { return dateCropOK }
    else { return 'invalid date'} 

} 

export default DateHandler;