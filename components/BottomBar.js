import React, { Component } from 'react'
import InputField from './InputField';

import './BottomBar.css'

export default class BottomBar extends Component {
    render() {
        return (
            <div className='bottom_bar' > 
                <div className='input_message_wrap'  >
                    <InputField  handleSentMessage={this.props.handleSentMessage}   placeholder={'Type something cool'} />
                </div>
            </div>

        )
    }
}
