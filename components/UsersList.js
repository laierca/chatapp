import React from 'react'
import { List, Header } from 'semantic-ui-react'
import DateHandler from './DateHandler';

import Avatar from 'react-avatar';

import './UsersList.css'

class UsersList extends React.Component {
    
    
    UsersList = () => (

        <List>
               { this.props.data ?   this.props.data.map((post,i)  => {
                return (
                    <List.Item   key={i} >
                        <Avatar name={post.author}   size={50} round  />
                        <div style={{ display: 'inline-block',  margin: '0px 0px 0px 20px', top: '20px', position:'relative'}}>
                        <List.Content>
                            <List.Header as='h3'>{post.author}</List.Header>
                            <List.Description>
                                Last message: &nbsp; &nbsp;
                              <p>  {   DateHandler({date: post._created})}  </p>
                            </List.Description>
                        </List.Content>
                        </div>
                    </List.Item>

                )
            }) : <h1> no messages</h1>
        
        }
        </List>
    )

    render() {

        return (
            <div className='users_list_wrap'>
              <Header as='h3' style={{padding: '13px 0 0 25px'}}>  Contacts  </Header>
              <hr />

                <div className='users_list' >
                {this.UsersList()} 
                </div>

            </div>
        )
    }
}


export default UsersList
