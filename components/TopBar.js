import React , {Component} from 'react'
import { Header, Icon } from 'semantic-ui-react'

export default class TopBar extends Component {
    render() {
        return (
            <div className='top_bar' style={{ position: 'fixed', height: '50px', width: '100%', backgroundColor: 'white', }}>
               <Header as='h4'   style={{margin: '5px 0 0 20px' }}>
                    <Icon size='mini' name='chat' />
                    <Header.Content>
                        SimpleChat
                         <Header.Subheader>Test non-realtime chat application</Header.Subheader>
                    </Header.Content>
                </Header>
                <hr />
            </div>
        )
    }
}
