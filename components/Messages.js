import React, { Component } from 'react'
import UserMessage from './UserMessage';
import MyMessage from './MyMessage';

export default class Messages extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isLoaded: false
        }
    }
    
    scrollToBottom = () => {
        setTimeout(() => {
            this.messagesEnd.scrollIntoView({ behavior: "smooth" })
        }, 500)
        
      }
       
      componentDidMount() {
        this.scrollToBottom();
      }
      
      componentDidUpdate() {
        this.scrollToBottom();
      }

    render() {

        const { data, myMessages } = this.props
            
            return (
                <div className='messages_list'>
    
                    {data ? data.map((post, i) => {
    
                        return (<UserMessage author={post.author} message={post.message} id={post._id} timestamp={post._created} key={i} />)
    
                    }) : <div>  <h1>no messages</h1> </div>}
    
                    {myMessages ? myMessages.map((post, i) => {
                        return (<MyMessage author={post.author} message={post.message} timestamp={post._created} key={i} />)
                    }) : null}
    
                    <div style={{ float: "left", clear: "both" }}
                       ref={el => (this.messagesEnd = el)}    
                       >
                    </div>
                </div>
            )
       

        
    }
}
