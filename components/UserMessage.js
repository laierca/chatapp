import React, { Component } from 'react'
import { MessageBox } from 'react-chat-elements';
//var timestamp = require('unix-timestamp')
import { Icon } from 'semantic-ui-react';
import Avatar from 'react-avatar';
import DateHandler from './DateHandler';


import 'react-chat-elements/dist/main.css';
import './UserMessage.css'
import MyModal from './Modal';


export default class UserMessage extends Component {
    constructor(props) {
        super(props)
    
        this.handleDelete = this.handleDelete.bind(this)
    }
    

    dateFunc() {
        const date = DateHandler({ date: this.props.timestamp })
        return date
    }
    handleDelete = () => {
        fetch('https://lajerca.website/__CMS/WEBDEV/api/collections/remove/SimpleChat', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                filter: {_id: this.props.id}
            })
        })
        .then(res=>res.json())
        .then(entry => console.log(entry));

        
    }

    render() {
        console.log('USERMESSAGE', this.props);

        // var d = new Date('2019-03-25')
        return (
            <div className='user_message' >

                <div className='message_avatar_left'>
                    <Avatar name={this.props.author} size={50} round />
                </div>
                <div className='message_box_left'>
                    <MessageBox
                        position={'left'}
                        type={'text'}
                        text={this.props.message}
                        //date={d}
                        dateString={this.dateFunc()}
                        title={this.props.author}
                        titleColor='grey'
                        status='received'
                    />
                    <div style={{ position: 'absolute', top: '60px', left: '-25px' }}   >
                      

                           <MyModal   handleDelete={ this.handleDelete}   />
                    </div>

                </div>
            </div>
        )
    }
}
