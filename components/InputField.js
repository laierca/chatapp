import React, { Component } from 'react'
import { Input, Button, Form } from 'semantic-ui-react'

var timestamp = require('unix-timestamp')

class InputField extends Component {

    constructor(props) {
        super(props)

        this.state = {
            author: '',
            message: ''
        }
        this.sendMessage = this.sendMessage.bind(this)
    }
    sendMessage = () => {
        //const messageDATA = { message: 'Test post message', author: 'AJEJE BRAZORF'}
        const { author, message } = this.state ? this.state : undefined
        const messageDATA = author && message ? { author: this.state.author, message: this.state.message } : null
        const messageDATA_local = { author: this.state.author, message: this.state.message, _created: timestamp.now() }

        fetch('https://lajerca.website/__CMS/WEBDEV/api/collections/save/SimpleChat', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ data: messageDATA })
        })
            .then(response => response.json())
            .then(res => console.log('after FETCH POST', res))
            .catch(function (error) {
                console.log('SEND MESSAGES ERROR', error)
            })

        this.props.handleSentMessage(messageDATA_local)

        setTimeout(() => {
            this.setState({ author: '', message: '' })

        }, 1000);
        

    }

    SendButton = () => <Button
        onClick={() => this.sendMessage()}
        disabled={this.state.message.length < 2 ? true : false}

    > Send    </Button>

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    render() {
        console.log('inputfield | RENDER | state', this.state);
        console.log('inputfield | RENDER | props', this.props);
        return (

            <Form   >
                <Form.Input
                    maxLength="20"
                    type="text"
                    iconPosition='left'
                    icon='user'
                    onChange={this.handleChange}
                    value={this.state.author}
                    name='author'
                    placeholder={'Your Name (max 20 char)'} />
                <Form.Input
                    maxLength="50"
                    type="text"
                    icon='chat'
                    iconPosition='left'
                    onChange={this.handleChange}
                    value={this.state.message}
                    disabled={this.state.author.length < 2 ? true : false}
                    name='message'
                    action={this.SendButton()}
                    placeholder={'Type Something Cool (max 50 char)'} />
            </Form>

        )
    }
}

export default InputField